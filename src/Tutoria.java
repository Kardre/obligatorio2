

public class Tutoria {
	
	int identificador;
	String pregunta;
	String alumno;
	String asignatura;
	boolean leido;

	public Tutoria(){
		
	}


	public Tutoria (int identificador,String pregunta, String alumno, String asignatura,
			boolean leido) {
		this.identificador = identificador;
		this.pregunta = pregunta;
		this.alumno = alumno;
		this.asignatura = asignatura;
		this.leido = leido;
	}



	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getAlumno() {
		return alumno;
	}

	public void setAlumno(String alumno) {
		this.alumno = alumno;
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public boolean isLeido() {
		return leido;
	}

	public void setLeido(boolean leido) {
		this.leido = leido;
	}

	@Override
	public String toString() {
		return "Tutoria numero " + identificador + " de " + alumno + " la tutoria es " + pregunta + " de la asignatura " + asignatura;
	}

}

