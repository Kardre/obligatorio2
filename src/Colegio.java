import java.util.ArrayList;

public class Colegio {

	ArrayList<Profesor> profesores= new ArrayList<Profesor>();
	ArrayList<Alumno> alumnos= new ArrayList<Alumno>();
	ArrayList<Tutoria> tutorias = new ArrayList<Tutoria>();

	public Colegio() {
		alumnos.add(new Alumno("Pepe", "12345678z",23 , 12345, "1�DaM"));
		alumnos.add(new Alumno("Luis", "12345678x",23 , 12344, "2�DaM"));
		alumnos.add(new Alumno("Ruben", "12345678y",23 , 12355, "1�DaW"));
		alumnos.add(new Alumno("Ana", "12345678w",23 , 12333, "2�DaW"));
		alumnos.add(new Alumno("Santi", "12345678v",23 , 12343, "2�DaM"));

		profesores.add(new Profesor("Carlos", "12345678a", 55, "Programacion"));
		profesores.add(new Profesor("Raul", "12345678b", 55, "Entornos"));
		profesores.add(new Profesor("Carmelo", "12345678c", 55, "Base de Datos"));
		
		tutorias.add(new Tutoria(1234, "Como va el tema, profe?", "Pepe", "Programacion", false));
		tutorias.add(new Tutoria(1233, "Como va el tema, profe?", "Luis", "Programacion", false));
		tutorias.add(new Tutoria(1244, "Como va el tema, profe?", "Luis", "Entornos", false));
		tutorias.add(new Tutoria(1223, "Ultimo, profe?", "Ana", "Programacion", true));

	}

	void listarProfesores() {
		for(int i=0;i < this.profesores.size();i++) {
			System.out.println((i+1) + " - " + this.profesores.get(i).nombre + " profesor de " + this.profesores.get(i).asignatura);
		}
	}
	
	public void a�adirTutorias (Tutoria tutoria) {
		tutorias.add(tutoria); 
	}

	public int buscarDni(String busqueda){
		int vacio = 0;
		for (int i= 0; i <this.alumnos.size(); i++) {
			Alumno actualEnElArray = this.alumnos.get(i);
			if(actualEnElArray.getDni().equals(busqueda)) {
				System.out.println("Bienvenido " + this.alumnos.get(i).nombre + " alumno de " + this.alumnos.get(i).curso);
				vacio = 1;
			} else if (vacio == 0) {
				for (int j=0;j<this.profesores.size();j++) {
					Profesor actualEnElArray1 = this.profesores.get(j);
					if(actualEnElArray1.getDni().equals(busqueda)) {
						System.out.println("Bienvenido " + this.profesores.get(j).nombre + " profesor de " + this.profesores.get(j).asignatura);
						vacio = 2;
					}
				}
			}
		}
		return vacio;
	}

	String darAsignatura (String busqueda) {
		String asignatura = "";
		for ( int i=0; i<profesores.size();i++) {
			Profesor actualEnElArray = this.profesores.get(i);
			if (actualEnElArray.getDni().equals(busqueda)) {
				asignatura = actualEnElArray.asignatura;
			}
		}
		return asignatura;
	}
	
	String darAlumno (String busqueda) {
		String nombre = "";
		for ( int i=0; i<alumnos.size();i++) {
			Alumno actualEnElArray = this.alumnos.get(i);
			if (actualEnElArray.getDni().equals(busqueda)) {
				nombre = actualEnElArray.nombre;
			}
		}
		return nombre;
	}
	
	public ArrayList<Tutoria> getTutoria() {
		return tutorias;
	}

	public void setTutoria(ArrayList<Tutoria> tutoria) {
		this.tutorias = tutoria;
	}
	
	
	void buscarTutoriaActivas(String asignatura){
		for (int i= 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray = this.tutorias.get(i);
			if(actualEnElArray.getAsignatura().equals(asignatura)&&!actualEnElArray.leido) {
				System.out.println(this.tutorias.get(i));
			}
		}
	}
	
	void buscarTutoriaActivasNombre(String alumno){
		for (int i= 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray = this.tutorias.get(i);
			if(actualEnElArray.getAlumno().equals(alumno)&&actualEnElArray.leido) {
				System.out.println(this.tutorias.get(i));
			} 
		}
	}

	Tutoria buscarTutoriasIdentificador(int ide) {
		Tutoria objeto = null;
		for (int i= 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray = this.tutorias.get(i);
			if(actualEnElArray.getIdentificador()==(ide)&&!actualEnElArray.leido) {
				System.out.println(this.tutorias.get(i));
				objeto = this.tutorias.get(i);
			}
		}
		return objeto;
	}
	
	Tutoria buscarTutoriasIdentificadorAlumno(int ide) {
		Tutoria objeto = null;
		for (int i= 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray = this.tutorias.get(i);
			if(actualEnElArray.getIdentificador()==(ide)&&actualEnElArray.leido) {
				System.out.println(this.tutorias.get(i));
				objeto = this.tutorias.get(i);
			}
		}
		return objeto;
	}

	void ponerEnLeido(int ide) {
		Tutoria tutoria = null;
		for (int i = 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray= this.tutorias.get(i);	
			if (actualEnElArray.getIdentificador()==ide&&!actualEnElArray.leido) {
				tutoria = actualEnElArray;
				tutoria.setLeido(true);
			}
		}
	}
	
	void ponerEnNoLeido(int ide) {
		Tutoria tutoria = null;
		for (int i = 0; i <this.tutorias.size(); i++) {
			Tutoria actualEnElArray= this.tutorias.get(i);	
			if (actualEnElArray.getIdentificador()==ide&&actualEnElArray.leido) {
				tutoria = actualEnElArray;
				tutoria.setLeido(false);
			}

		}
	}
}






