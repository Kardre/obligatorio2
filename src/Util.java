
import java.util.Scanner;

//mi prototipo de pagina de utiles que planeo ir llenando e ir usando a lo largo del curso
public class Util {
	static Scanner scanner = new Scanner(System.in);
	public int validateRange(int min, int max) {

		boolean isValidOpcion = false;

		int result = -1;
		while (!isValidOpcion) {
			int opcion = scanner.nextInt();
			scanner.nextLine();
			if ( opcion < min || opcion > max) {
				System.out.println("Introduzca una opcion entre " + min + " y " + max + ".");
			}else {
				result = opcion;
				isValidOpcion = true;
			}
		}
		return result;
	}

	static public String validateString() {
		return scanner.nextLine();

	}

	public static boolean validarNombre(String nombre) {

		boolean valido = true;
		
		String letras = "abcdefghijklmn�opqrstuvwxyz";

		for (int z = 0; z<nombre.length();z++) {
			
			if (letras.indexOf(nombre.toLowerCase().charAt(z)) == -1) {
				valido = false;
			}
		}
		return valido;
	}
	
	 static boolean validarNumero(String edad) {

		boolean valido = true;
		String numeros = "0123456789";

		for (int z = 0; z<edad.length();z++) {
			
			if (numeros.indexOf(edad.charAt(z)) == -1) {
				valido = false;
			}
		}
		return valido;
	}
	
}
