import java.util.Scanner;

public class Menu {

	Tutoria miTutoria = new Tutoria();
	Colegio miColegio = new Colegio();
	Scanner scanner = new Scanner(System.in);

	public void menuAcceso() {

		System.out.println("Introduzca su DNI por favor.");

		String busqueda = new Util().validateString();
		int opcion = miColegio.buscarDni(busqueda);

		if (opcion == 1) {
			menuAlumno(busqueda);
		} else if (opcion == 2) {
			menuProfesor(busqueda);
		} else if (opcion ==0 ) {
			System.out.println("Persona no encontrada");
			menuAcceso();
		}

	}


	private void menuProfesor(String busqueda) {
		String asignatura = miColegio.darAsignatura(busqueda);
		System.out.println("1- Consultar tutorias pendientes \n2- Responder tutorias \n3- Menu de acceso");
		int opcion = new Util().validateRange(1, 3);
		if (opcion == 1) {
			miColegio.buscarTutoriaActivas(asignatura);	
			menuProfesor(busqueda);
		}else if (opcion == 2) {
			responderTutorias();
			menuProfesor(busqueda);
		}else if (opcion == 3) {
			menuAcceso();
		}

	}

	private void menuAlumno(String busqueda) {
		String nombre =miColegio.darAlumno(busqueda);
		System.out.println("1- Mandar tutorias \n2- Ver respuesta a las tutorias \n3- Menu de acceso");
		int opcion = new Util().validateRange(1, 3);
		if (opcion == 1) {
			mandarTutoria(nombre);
			menuAlumno(busqueda);
		}else if (opcion == 2) {
			miColegio.buscarTutoriaActivasNombre(nombre);
			System.out.println("Quiere responder \n1- Si\n2- No");
			menuRespuestaAlumno(busqueda);
		}else if(opcion ==3) {
			menuAcceso();
		}
	}

	void menuRespuestaAlumno(String busqueda) {
		String nombre =miColegio.darAlumno(busqueda);
		int opcion = new Util().validateRange(1, 2);
		if (opcion == 1) {
			responderTutoriasAlumno();
			menuAlumno(busqueda);

		} else if (opcion ==2) {
			menuAlumno(busqueda);

		}

	}

	private void responderTutorias() {

		int identificador;
		String alumno;
		String asignatura;
		boolean leido;
		System.out.println("Introduzca identificador de Tutoria");
		int ide = scanner.nextInt();
		Tutoria objeto = miColegio.buscarTutoriasIdentificador(ide);
		identificador = objeto.getIdentificador();
		alumno = objeto.getAlumno();
		asignatura = objeto.getAsignatura();
		leido = objeto.leido;
		System.out.println("Introduzca su respuesta");
		String pregunta = "La respuesda del profesor es: "+ "'"  + new Util().validateString() +"' ";
		Tutoria tutoria= new Tutoria(identificador, pregunta, alumno, asignatura, leido);
		miColegio.aņadirTutorias(tutoria);
		System.out.println("Tutoria respondida");
		miColegio.ponerEnLeido(ide);
	}
	private void responderTutoriasAlumno() {

		int identificador;
		String alumno;
		String asignatura;
		boolean leido;
		System.out.println("Introduzca identificador de Tutoria");
		int ide = scanner.nextInt();
		Tutoria objeto = miColegio.buscarTutoriasIdentificadorAlumno(ide);
		identificador = objeto.getIdentificador();
		alumno = objeto.getAlumno();
		asignatura = objeto.getAsignatura();
		leido = objeto.leido;
		System.out.println("Introduzca su respuesta");
		String pregunta = "La respuesda del alumno es: " + new Util().validateString();
		Tutoria tutoria= new Tutoria(identificador, pregunta, alumno, asignatura, leido);
		miColegio.aņadirTutorias(tutoria);
		System.out.println("Tutoria respondida");
		miColegio.ponerEnNoLeido(ide);
	}

	private void mandarTutoria(String nombre) {
		System.out.println("A que profesor desea mandarle la tutoria");
		miColegio.listarProfesores();
		int opcion = new Util().validateRange(1, 3);
		int identificador = miColegio.tutorias.size();
		System.out.println("Haga su consulta");
		String pregunta = new Util().validateString();
		String alumno = nombre;
		String asignatura = miColegio.profesores.get(opcion-1).asignatura;
		boolean leido = false;

		Tutoria tutoria = new Tutoria(identificador, pregunta, alumno, asignatura, leido);
		miColegio.aņadirTutorias(tutoria);


	}

}







