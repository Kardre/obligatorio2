
public class Alumno extends Persona {
	int matricula;
	String curso;
	

	public Alumno(String nombre, String dni, int edad, int matricula, String curso) {
		super(nombre, dni, edad);
		this.matricula = matricula;
		this.curso = curso;
	}

	public int getMatricula() {
		return matricula;
	}


	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}


	public String getCurso() {
		return curso;
	}


	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	
	
}
